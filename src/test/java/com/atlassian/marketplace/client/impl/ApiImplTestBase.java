package com.atlassian.marketplace.client.impl;

import org.junit.Before;

import static com.atlassian.marketplace.client.TestObjects.BASE_URI;
import static com.atlassian.marketplace.client.impl.ClientTester.defaultRootResource;

public abstract class ApiImplTestBase
{
    protected ClientTester tester;
    
    @Before
    public void setUp() throws Exception
    {
        tester = new ClientTester(BASE_URI);
        tester.mockResource(tester.apiBase, defaultRootResource());
    }
}
