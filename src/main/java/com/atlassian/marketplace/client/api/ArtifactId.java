package com.atlassian.marketplace.client.api;

import java.net.URI;

/**
 * Specifies an add-on artifact (such as a .jar file).  This can be obtained by
 * uploading an artifact file through the {@link Assets} API, or by providing the URI
 * of an artifact that is available online.
 * @since 2.0.0
 */
public final class ArtifactId extends ResourceId
{
    private ArtifactId(URI value)
    {
        super(value);
    }

    /**
     * Specifies the URI of an artifact that is available online.  The Marketplace server
     * will download the artifact when it creates or updates the add-on that uses it.
     */
    public static ArtifactId fromUri(URI uri)
    {
        return new ArtifactId(uri);
    }
}
